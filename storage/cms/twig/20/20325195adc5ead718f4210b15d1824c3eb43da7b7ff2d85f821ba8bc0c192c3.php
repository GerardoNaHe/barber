<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/partials/header.htm */
class __TwigTemplate_8a9adf49d38062c83c00d42d6c89ebf0c5ca4995ca8bc75f93e9fbc4d289c366 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!--? Header Start -->
        <div class=\"header-area header-transparent pt-20\">
            <div class=\"main-header header-sticky\">
                <div class=\"container-fluid\">
                    <div class=\"row align-items-center\">
                        <!-- Logo -->
                        <div class=\"col-xl-2 col-lg-2 col-md-1\">
                            <div class=\"logo\">
                                <a href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("Index");
        echo "\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/logo/logo.png");
        echo "\" alt=\"\"></a>
                            </div>
                        </div>
                        <div class=\"col-xl-10 col-lg-10 col-md-10\">
                            <div class=\"menu-main d-flex align-items-center justify-content-end\">
                                <!-- Main-menu -->
                                <div class=\"main-menu f-right d-none d-lg-block\">

                                    <nav>
                                      <!--  <ul id=\"navigation\">
        <li class=\"";
        // line 19
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 19), "id", [], "any", false, false, false, 19) == "Index")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("Index");
        echo "\">Home</a></li>
        <li class=\"";
        // line 20
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20) == "about")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">About</a></li>
        <li class=\"";
        // line 21
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21) == "services")) {
            echo "active";
        }
        echo "\"><a href=\"/services\">Services</a></li>
        <li class=\"";
        // line 22
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 22), "id", [], "any", false, false, false, 22) == "portfolio")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("portfolio");
        echo "\">Portfolio</a></li>
        <li class=\"";
        // line 23
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 23), "id", [], "any", false, false, false, 23) == "blog")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\">Blog</a>
                                                <ul class=\"submenu\">
                                                    <li><a href=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\">Blog</a></li>
                                                    <li><a href=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog-details");
        echo "\">Blog Details</a></li>
                                                    <li><a href=\"";
        // line 27
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("element");
        echo "\">Element</a></li>
                                                </ul>
                                            </li>
                                            <li><a href=\"";
        // line 30
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li>
                                        </ul> -->  
                                 
                                 ";
        // line 33
        if (twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 33)) {
            // line 34
            echo "    <ul>
        ";
            // line 35
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['items'] = twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 35)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["staticMenu"] ?? null) . "::items")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 36
            echo "    </ul>
";
        }
        // line 38
        echo "                                 
                                    </nav>


                                </div>
                                <div class=\"header-right-btn f-right d-none d-lg-block ml-30\">
                                    <a href=\"from.html\" class=\"btn header-btn\">became a member</a>
                                </div>
                            </div>
                        </div>   
                        <!-- Mobile Menu -->
                        <div class=\"col-12\">
                            <div class=\"mobile_menu d-block d-lg-none\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 38,  131 => 36,  126 => 35,  123 => 34,  121 => 33,  115 => 30,  109 => 27,  105 => 26,  101 => 25,  92 => 23,  84 => 22,  78 => 21,  70 => 20,  62 => 19,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!--? Header Start -->
        <div class=\"header-area header-transparent pt-20\">
            <div class=\"main-header header-sticky\">
                <div class=\"container-fluid\">
                    <div class=\"row align-items-center\">
                        <!-- Logo -->
                        <div class=\"col-xl-2 col-lg-2 col-md-1\">
                            <div class=\"logo\">
                                <a href=\"{{ 'Index'|page }}\"><img src=\"{{'assets/img/logo/logo.png'|theme}}\" alt=\"\"></a>
                            </div>
                        </div>
                        <div class=\"col-xl-10 col-lg-10 col-md-10\">
                            <div class=\"menu-main d-flex align-items-center justify-content-end\">
                                <!-- Main-menu -->
                                <div class=\"main-menu f-right d-none d-lg-block\">

                                    <nav>
                                      <!--  <ul id=\"navigation\">
        <li class=\"{% if this.page.id == 'Index' %}active{% endif %}\"><a href=\"{{ 'Index'|page }}\">Home</a></li>
        <li class=\"{% if this.page.id == 'about' %}active{% endif %}\"><a href=\"{{ 'about'|page }}\">About</a></li>
        <li class=\"{% if this.page.id == 'services' %}active{% endif %}\"><a href=\"/services\">Services</a></li>
        <li class=\"{% if this.page.id == 'portfolio' %}active{% endif %}\"><a href=\"{{ 'portfolio'|page }}\">Portfolio</a></li>
        <li class=\"{% if this.page.id == 'blog' %}active{% endif %}\"><a href=\"{{ 'blog'|page }}\">Blog</a>
                                                <ul class=\"submenu\">
                                                    <li><a href=\"{{ 'blog'|page }}\">Blog</a></li>
                                                    <li><a href=\"{{ 'blog-details'|page}}\">Blog Details</a></li>
                                                    <li><a href=\"{{ 'element'|page }}\">Element</a></li>
                                                </ul>
                                            </li>
                                            <li><a href=\"{{ 'contact'|page }}\">Contact</a></li>
                                        </ul> -->  
                                 
                                 {% if staticMenu.menuItems %}
    <ul>
        {% partial staticMenu ~ \"::items\" items=staticMenu.menuItems %}
    </ul>
{% endif %}
                                 
                                    </nav>


                                </div>
                                <div class=\"header-right-btn f-right d-none d-lg-block ml-30\">
                                    <a href=\"from.html\" class=\"btn header-btn\">became a member</a>
                                </div>
                            </div>
                        </div>   
                        <!-- Mobile Menu -->
                        <div class=\"col-12\">
                            <div class=\"mobile_menu d-block d-lg-none\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/partials/header.htm", "");
    }
}
