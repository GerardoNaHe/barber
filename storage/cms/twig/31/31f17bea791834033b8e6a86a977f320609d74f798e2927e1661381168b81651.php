<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/about.htm */
class __TwigTemplate_fcad8a4c70e1dbfa39f0e58402e81327f1633628bc7f80d3932cf8e8244af8bd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"preloader-active\">
        <div class=\"preloader d-flex align-items-center justify-content-center\">
            <div class=\"preloader-inner position-relative\">
                <div class=\"preloader-circle\"></div>
                <div class=\"preloader-img pere-text\">
                    <img src=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/logo/loder.png");
        echo "\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
   
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>About US</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? About Area Start -->
        <section class=\"about-area section-padding30 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row align-items-center\">
                    <div class=\"col-lg-6 col-md-11\">
                        <!-- about-img -->
                        <div class=\"about-img \">
                            <img src=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/about.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                    <div class=\"col-lg-6 col-md-12\">
                        <div class=\"about-caption\">
                            <!-- Section Tittle -->
                            <div class=\"section-tittle section-tittle3 mb-35\">
                                <span>About Our company</span>
                                <h2>52 Years Of Experience In Hair cut!</h2>
                            </div>
                            <p class=\"mb-30 pera-bottom\">Brook presents your services with flexible, convenient and cdpoe layouts. You can select your favorite layouts & elements for cular ts with unlimited ustomization possibilities. Pixel-perfreplication of the designers is intended.</p>
                            <p class=\"pera-top mb-50\">Brook presents your services with flexible, convefnient and ent anipurpose layouts. You can select your favorite.</p>
                            <img src=\"";
        // line 45
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/signature.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Shape -->
            <div class=\"about-shape\">
                <img src=\"";
        // line 52
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/about-shape.png");
        echo "\" alt=\"\">
            </div>
        </section>
        <!-- About-2 Area End -->
        <!--? Services Area Start -->
        <section class=\"service-area pb-170\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                <div class=\"row\">
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Stylish Hair Cut</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption active text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Body Massege</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div> 
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Breard Style</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services Area End -->
        <!--? Team Start -->
        <div class=\"team-area pb-180\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-8 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>Professional Teams</span>
                            <h2>Our award winner hair cut exparts for you</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row team-active dot-style\">
                    <!-- single Tem -->
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 124
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team1.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Guy C. Pulido bks</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 135
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team2.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Color Expart</span>
                                <h3><a href=\"#\">Steve L. Nolan</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 146
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team3.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 157
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team2.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!-- Best Pricing Area Start -->
        <div class=\"best-pricing section-padding2 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row justify-content-end\">
                    <div class=\"col-xl-7 col-lg-7\">
                        <div class=\"section-tittle mb-50\">
                            <span>Our Best Pricing</span>
                            <h2>We provide best price<br> in the city!</h2>
                        </div>
                        <!-- Pricing  -->
                        <div class=\"row\">
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Styling. . . . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$25</span></li>
                                        <li>Styling + Color. . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Styling + Tint. . . . . . . . . . . . . . . . . . . . . .<span>\$65</span></li>
                                        <li>  Semi-permanent wave. . . . . . . . . . . . .<span>\$65</span></li>
                                        <li> Cut + Styling. . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li> Cut + Styling + Color. . . . . . . . . . . . . <span>\$100</span></li>
                                        <li> Cut + Styling + Tint. . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Cut. . . . . . . . . . . . . . . . . . . . . . . . . . . . .<span>\$25</span></li>
                                        <li>Shave. . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Beard trim. . . . . . . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + beard trim. . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + shave. . . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li>Clean up. . . . . . . . . . . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- pricing img -->
            <div class=\"pricing-img\">
                <img class=\"pricing-img1\" src=\"";
        // line 211
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/pricing1.png");
        echo "\" alt=\"\">
                <img class=\"pricing-img2\" src=\"";
        // line 212
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/pricing2.png");
        echo "\" alt=\"\">
            </div>
        </div>
        <!-- Best Pricing Area End -->
        <!--? Gallery Area Start -->
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-9 col-sm-10\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>our image gellary</span>
                            <h2>some images from our barber shop</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 231
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery1.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 237
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery2.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 243
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery3.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 249
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery4.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 249,  317 => 243,  308 => 237,  299 => 231,  277 => 212,  273 => 211,  216 => 157,  202 => 146,  188 => 135,  174 => 124,  99 => 52,  89 => 45,  74 => 33,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"preloader-active\">
        <div class=\"preloader d-flex align-items-center justify-content-center\">
            <div class=\"preloader-inner position-relative\">
                <div class=\"preloader-circle\"></div>
                <div class=\"preloader-img pere-text\">
                    <img src=\"{{'assets/img/logo/loder.png'|theme}}\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
   
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>About US</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? About Area Start -->
        <section class=\"about-area section-padding30 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row align-items-center\">
                    <div class=\"col-lg-6 col-md-11\">
                        <!-- about-img -->
                        <div class=\"about-img \">
                            <img src=\"{{'assets/img/gallery/about.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                    <div class=\"col-lg-6 col-md-12\">
                        <div class=\"about-caption\">
                            <!-- Section Tittle -->
                            <div class=\"section-tittle section-tittle3 mb-35\">
                                <span>About Our company</span>
                                <h2>52 Years Of Experience In Hair cut!</h2>
                            </div>
                            <p class=\"mb-30 pera-bottom\">Brook presents your services with flexible, convenient and cdpoe layouts. You can select your favorite layouts & elements for cular ts with unlimited ustomization possibilities. Pixel-perfreplication of the designers is intended.</p>
                            <p class=\"pera-top mb-50\">Brook presents your services with flexible, convefnient and ent anipurpose layouts. You can select your favorite.</p>
                            <img src=\"{{'assets/img/gallery/signature.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Shape -->
            <div class=\"about-shape\">
                <img src=\"{{'assets/img/gallery/about-shape.png'|theme}}\" alt=\"\">
            </div>
        </section>
        <!-- About-2 Area End -->
        <!--? Services Area Start -->
        <section class=\"service-area pb-170\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                <div class=\"row\">
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Stylish Hair Cut</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption active text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Body Massege</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div> 
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Breard Style</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services Area End -->
        <!--? Team Start -->
        <div class=\"team-area pb-180\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-8 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>Professional Teams</span>
                            <h2>Our award winner hair cut exparts for you</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row team-active dot-style\">
                    <!-- single Tem -->
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team1.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Guy C. Pulido bks</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team2.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Color Expart</span>
                                <h3><a href=\"#\">Steve L. Nolan</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team3.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team2.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!-- Best Pricing Area Start -->
        <div class=\"best-pricing section-padding2 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row justify-content-end\">
                    <div class=\"col-xl-7 col-lg-7\">
                        <div class=\"section-tittle mb-50\">
                            <span>Our Best Pricing</span>
                            <h2>We provide best price<br> in the city!</h2>
                        </div>
                        <!-- Pricing  -->
                        <div class=\"row\">
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Styling. . . . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$25</span></li>
                                        <li>Styling + Color. . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Styling + Tint. . . . . . . . . . . . . . . . . . . . . .<span>\$65</span></li>
                                        <li>  Semi-permanent wave. . . . . . . . . . . . .<span>\$65</span></li>
                                        <li> Cut + Styling. . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li> Cut + Styling + Color. . . . . . . . . . . . . <span>\$100</span></li>
                                        <li> Cut + Styling + Tint. . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Cut. . . . . . . . . . . . . . . . . . . . . . . . . . . . .<span>\$25</span></li>
                                        <li>Shave. . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Beard trim. . . . . . . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + beard trim. . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + shave. . . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li>Clean up. . . . . . . . . . . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- pricing img -->
            <div class=\"pricing-img\">
                <img class=\"pricing-img1\" src=\"{{'assets/img/gallery/pricing1.png'|theme}}\" alt=\"\">
                <img class=\"pricing-img2\" src=\"{{'assets/img/gallery/pricing2.png'|theme}}\" alt=\"\">
            </div>
        </div>
        <!-- Best Pricing Area End -->
        <!--? Gallery Area Start -->
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-9 col-sm-10\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>our image gellary</span>
                            <h2>some images from our barber shop</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery1.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery2.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery3.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery4.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/about.htm", "");
    }
}
