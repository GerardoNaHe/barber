<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/blog.htm */
class __TwigTemplate_d8236e51acea348d89133f80f5263f92fb8145e1d85ea2a40e87d9489d3a27b8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["post"] = twig_get_attribute($this->env, $this->source, ($context["newsPosts"] ?? null), "posts", [], "any", false, false, false, 1);
        // line 2
        echo "
    <main>
        <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Blog</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Blog Area Start-->
        <section class=\"blog_area section-padding\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-lg-8 mb-5 mb-lg-0\">
                        <div class=\"blog_left_sidebar\">

                            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 27
            echo "
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"";
            // line 30
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 30));
            echo "\" alt=\"\">
                                    <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 31), "html", null, true);
            echo "\" class=\"blog_item_date\">
                                        <h3>7</h3>
                                        <p>Julio</p>
                                    </a>
                                </div>

                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 38), "html", null, true);
            echo "\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 39), "html", null, true);
            echo "</h2>
                                    </a>

                                    ";
            // line 42
            echo twig_get_attribute($this->env, $this->source, $context["post"], "introductory", [], "any", false, false, false, 42);
            echo "
                                    ";
            // line 43
            echo twig_get_attribute($this->env, $this->source, $context["post"], "content", [], "any", false, false, false, 43);
            echo "

                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>

                            </article>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"";
        // line 57
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog/single_blog_2.png");
        echo "\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"";
        // line 77
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog/single_blog_3.png");
        echo "\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"";
        // line 97
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog/single_blog_4.png");
        echo "\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"";
        // line 117
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog/single_blog_5.png");
        echo "\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <nav class=\"blog-pagination justify-content-center d-flex\">
                                <ul class=\"pagination\">
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\" aria-label=\"Previous\">
                                            <i class=\"ti-angle-left\"></i>
                                        </a>
                                    </li>
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\">1</a>
                                    </li>
                                    <li class=\"page-item active\">
                                        <a href=\"#\" class=\"page-link\">2</a>
                                    </li>
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\" aria-label=\"Next\">
                                            <i class=\"ti-angle-right\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-lg-4\">
                        <div class=\"blog_right_sidebar\">
                            <aside class=\"single_sidebar_widget search_widget\">
                                <form action=\"#\">
                                    <div class=\"form-group\">
                                        <div class=\"input-group mb-3\">
                                            <input type=\"text\" class=\"form-control\" placeholder='Search Keyword'
                                                onfocus=\"this.placeholder = ''\"
                                                onblur=\"this.placeholder = 'Search Keyword'\">
                                            <div class=\"input-group-append\">
                                                <button class=\"btns\" type=\"button\"><i class=\"ti-search\"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <button class=\"button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn\"
                                        type=\"submit\">Search</button>
                                </form>
                            </aside>
                            <aside class=\"single_sidebar_widget post_category_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Category</h4>
                                <ul class=\"list cat-list\">
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Resaurant food</p>
                                            <p>(37)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Travel news</p>
                                            <p>(10)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Modern technology</p>
                                            <p>(03)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Product</p>
                                            <p>(11)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Inspiration</p>
                                            <p>21</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Health Care (21)</p>
                                            <p>09</p>
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                            <aside class=\"single_sidebar_widget popular_post_widget\">
                                <h3 class=\"widget_title\" style=\"color: #2d2d2d;\">Recent Post</h3>
                                <div class=\"media post_item\">
                                    <img src=\"";
        // line 219
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_1.png");
        echo "\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">From life was you fish...</h3>
                                        </a>
                                        <p>January 12, 2019</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"";
        // line 228
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_2.png");
        echo "\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">The Amazing Hubble</h3>
                                        </a>
                                        <p>02 Hours ago</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"";
        // line 237
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_3.png");
        echo "\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">Astronomy Or Astrology</h3>
                                        </a>
                                        <p>03 Hours ago</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"";
        // line 246
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_4.png");
        echo "\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">Asteroids telescope</h3>
                                        </a>
                                        <p>01 Hours ago</p>
                                    </div>
                                </div>
                            </aside>
                            <aside class=\"single_sidebar_widget tag_cloud_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Tag Clouds</h4>
                                <ul class=\"list\">
                                    <li>
                                        <a href=\"#\">project</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">love</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">technology</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">travel</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">restaurant</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">life style</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">design</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">illustration</a>
                                    </li>
                                </ul>
                            </aside>

                            <aside class=\"single_sidebar_widget instagram_feeds\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Instagram Feeds</h4>
                                <ul class=\"instagram_row flex-wrap\">
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 290
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_5.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 295
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_6.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 300
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_7.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 305
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_8.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 310
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_9.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"";
        // line 315
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/post/post_10.png");
        echo "\" alt=\"\">
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                            <aside class=\"single_sidebar_widget newsletter_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Newsletter</h4>
                                <form action=\"#\">
                                    <div class=\"form-group\">
                                        <input type=\"email\" class=\"form-control\" onfocus=\"this.placeholder = ''\"
                                            onblur=\"this.placeholder = 'Enter email'\" placeholder='Enter email' required>
                                    </div>
                                    <button class=\"button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn\"
                                        type=\"submit\">Subscribe</button>
                                </form>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Area End -->
    </main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  421 => 315,  413 => 310,  405 => 305,  397 => 300,  389 => 295,  381 => 290,  334 => 246,  322 => 237,  310 => 228,  298 => 219,  193 => 117,  170 => 97,  147 => 77,  124 => 57,  119 => 54,  102 => 43,  98 => 42,  92 => 39,  88 => 38,  78 => 31,  74 => 30,  69 => 27,  65 => 26,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{%set post = newsPosts.posts %}

    <main>
        <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Blog</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Blog Area Start-->
        <section class=\"blog_area section-padding\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-lg-8 mb-5 mb-lg-0\">
                        <div class=\"blog_left_sidebar\">

                            {% for post in posts %}

                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"{{ post.image|media}}\" alt=\"\">
                                    <a href=\"{{ post.url }}\" class=\"blog_item_date\">
                                        <h3>7</h3>
                                        <p>Julio</p>
                                    </a>
                                </div>

                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"{{ post.url }}\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">{{ post.title }}</h2>
                                    </a>

                                    {{ post.introductory|raw }}
                                    {{ post.content|raw }}

                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>

                            </article>

                            {% endfor %}

                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"{{'assets/img/blog/single_blog_2.png'|theme}}\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"{{'assets/img/blog/single_blog_3.png'|theme}}\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"{{'assets/img/blog/single_blog_4.png'|theme}}\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article class=\"blog_item\">
                                <div class=\"blog_item_img\">
                                    <img class=\"card-img rounded-0\" src=\"{{'assets/img/blog/single_blog_5.png'|theme}}\" alt=\"\">
                                    <a href=\"#\" class=\"blog_item_date\">
                                        <h3>15</h3>
                                        <p>Jan</p>
                                    </a>
                                </div>
                                <div class=\"blog_details\">
                                    <a class=\"d-inline-block\" href=\"blog_details.html\">
                                        <h2 class=\"blog-head\" style=\"color: #2d2d2d;\">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>That dominion stars lights dominion divide years for fourth have don't stars is that
                                        he earth it first without heaven in place seed it second morning saying.</p>
                                    <ul class=\"blog-info-link\">
                                        <li><a href=\"#\"><i class=\"fa fa-user\"></i> Travel, Lifestyle</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-comments\"></i> 03 Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                            <nav class=\"blog-pagination justify-content-center d-flex\">
                                <ul class=\"pagination\">
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\" aria-label=\"Previous\">
                                            <i class=\"ti-angle-left\"></i>
                                        </a>
                                    </li>
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\">1</a>
                                    </li>
                                    <li class=\"page-item active\">
                                        <a href=\"#\" class=\"page-link\">2</a>
                                    </li>
                                    <li class=\"page-item\">
                                        <a href=\"#\" class=\"page-link\" aria-label=\"Next\">
                                            <i class=\"ti-angle-right\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-lg-4\">
                        <div class=\"blog_right_sidebar\">
                            <aside class=\"single_sidebar_widget search_widget\">
                                <form action=\"#\">
                                    <div class=\"form-group\">
                                        <div class=\"input-group mb-3\">
                                            <input type=\"text\" class=\"form-control\" placeholder='Search Keyword'
                                                onfocus=\"this.placeholder = ''\"
                                                onblur=\"this.placeholder = 'Search Keyword'\">
                                            <div class=\"input-group-append\">
                                                <button class=\"btns\" type=\"button\"><i class=\"ti-search\"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <button class=\"button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn\"
                                        type=\"submit\">Search</button>
                                </form>
                            </aside>
                            <aside class=\"single_sidebar_widget post_category_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Category</h4>
                                <ul class=\"list cat-list\">
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Resaurant food</p>
                                            <p>(37)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Travel news</p>
                                            <p>(10)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Modern technology</p>
                                            <p>(03)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Product</p>
                                            <p>(11)</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Inspiration</p>
                                            <p>21</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"d-flex\">
                                            <p>Health Care (21)</p>
                                            <p>09</p>
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                            <aside class=\"single_sidebar_widget popular_post_widget\">
                                <h3 class=\"widget_title\" style=\"color: #2d2d2d;\">Recent Post</h3>
                                <div class=\"media post_item\">
                                    <img src=\"{{'assets/img/post/post_1.png'|theme}}\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">From life was you fish...</h3>
                                        </a>
                                        <p>January 12, 2019</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"{{'assets/img/post/post_2.png'|theme}}\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">The Amazing Hubble</h3>
                                        </a>
                                        <p>02 Hours ago</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"{{'assets/img/post/post_3.png'|theme}}\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">Astronomy Or Astrology</h3>
                                        </a>
                                        <p>03 Hours ago</p>
                                    </div>
                                </div>
                                <div class=\"media post_item\">
                                    <img src=\"{{'assets/img/post/post_4.png'|theme}}\" alt=\"post\">
                                    <div class=\"media-body\">
                                        <a href=\"blog_details.html\">
                                            <h3 style=\"color: #2d2d2d;\">Asteroids telescope</h3>
                                        </a>
                                        <p>01 Hours ago</p>
                                    </div>
                                </div>
                            </aside>
                            <aside class=\"single_sidebar_widget tag_cloud_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Tag Clouds</h4>
                                <ul class=\"list\">
                                    <li>
                                        <a href=\"#\">project</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">love</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">technology</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">travel</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">restaurant</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">life style</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">design</a>
                                    </li>
                                    <li>
                                        <a href=\"#\">illustration</a>
                                    </li>
                                </ul>
                            </aside>

                            <aside class=\"single_sidebar_widget instagram_feeds\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Instagram Feeds</h4>
                                <ul class=\"instagram_row flex-wrap\">
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_5.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_6.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_7.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_8.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_9.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <img class=\"img-fluid\" src=\"{{'assets/img/post/post_10.png'|theme}}\" alt=\"\">
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                            <aside class=\"single_sidebar_widget newsletter_widget\">
                                <h4 class=\"widget_title\" style=\"color: #2d2d2d;\">Newsletter</h4>
                                <form action=\"#\">
                                    <div class=\"form-group\">
                                        <input type=\"email\" class=\"form-control\" onfocus=\"this.placeholder = ''\"
                                            onblur=\"this.placeholder = 'Enter email'\" placeholder='Enter email' required>
                                    </div>
                                    <button class=\"button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn\"
                                        type=\"submit\">Subscribe</button>
                                </form>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Area End -->
    </main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/blog.htm", "");
    }
}
