<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/portfolio.htm */
class __TwigTemplate_d3296b5573e9b77ffdd411970d4ddd6bd38c648ded67093886ed0182f8b129d2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<main>
        <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Portfolio</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Gallery Area Start -->
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery1.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery2.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 35
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery3.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 41
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery4.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
    </main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/portfolio.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 41,  79 => 35,  70 => 29,  61 => 23,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<main>
        <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Portfolio</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Gallery Area Start -->
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery1.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery2.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery3.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery4.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
    </main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/portfolio.htm", "");
    }
}
