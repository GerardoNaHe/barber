<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/services.htm */
class __TwigTemplate_00e4e3d17f8e76b8e923950245536c794eac9e9862da09362cfd02819b11cd4b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<main>
 <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Our Services</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Services Area Start -->
        <section class=\"service-area section-padding30\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                            
                            
                            
";
        // line 30
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "records", [], "any", false, false, false, 30);
        // line 31
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "displayColumn", [], "any", false, false, false, 31);
        // line 32
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "noRecordsMessage", [], "any", false, false, false, 32);
        // line 33
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsPage", [], "any", false, false, false, 33);
        // line 34
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 34);
        // line 35
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 35);
        // line 36
        echo "

    ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 39
            echo "        <h4><a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("services", ["slug" => twig_get_attribute($this->env, $this->source, $context["record"], "slug", [], "any", false, false, false, 39)]);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "title", [], "any", false, false, false, 39), "html", null, true);
            echo "</a></h4>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "          
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                

    <div class=\"row\">
    ";
        // line 48
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 48);
        // line 49
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 49);
        // line 50
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 50);
        // line 51
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 51);
        // line 52
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 52);
        // line 53
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 53);
        // line 54
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 54);
        // line 55
        echo "    
    ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 57
            echo "
        <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                 <h4><a href=\"";
            // line 64
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("service", ["slug" => twig_get_attribute($this->env, $this->source, $context["record"], "slug", [], "any", false, false, false, 64)]);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "title", [], "any", false, false, false, 64), "html", null, true);
            echo "</a></h4>
                                <h4>";
            // line 65
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "category", [], "any", false, false, false, 65), "title", [], "any", false, false, false, 65), "html", null, true);
            echo "</h4>

                                <p>";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 67), "html", null, true);
            echo ".</p>
                                <p>";
            // line 68
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "date", [], "any", false, false, false, 68), "Y-m-d h:i"), "html", null, true);
            echo ".</p>
                            </div>
                        </div>
                    </div>
              
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "    
    </div>

   </div>
        </section>
    </main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 74,  154 => 68,  150 => 67,  145 => 65,  139 => 64,  130 => 57,  126 => 56,  123 => 55,  121 => 54,  119 => 53,  117 => 52,  115 => 51,  113 => 50,  111 => 49,  109 => 48,  99 => 40,  88 => 39,  84 => 38,  80 => 36,  78 => 35,  76 => 34,  74 => 33,  72 => 32,  70 => 31,  68 => 30,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<main>
 <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>Our Services</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Services Area Start -->
        <section class=\"service-area section-padding30\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                            
                            
                            
{% set records = builderList2.records %}
{% set displayColumn = builderList2.displayColumn %}
{% set noRecordsMessage = builderList2.noRecordsMessage %}
{% set detailsPage = builderList2.detailsPage %}
{% set detailsKeyColumn = builderList2.detailsKeyColumn %}
{% set detailsUrlParameter = builderList2.detailsUrlParameter %}


    {% for record in records %}
        <h4><a href=\"{{'services'|page({slug:record.slug})}}\">{{record.title}}</a></h4>
    {% endfor %}          
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                

    <div class=\"row\">
    {% set records = builderList.records %}
{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}
    
    {% for record in records %}

        <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                 <h4><a href=\"{{'service'|page({slug:record.slug})}}\">{{record.title}}</a></h4>
                                <h4>{{record.category.title}}</h4>

                                <p>{{record.description}}.</p>
                                <p>{{record.date|date('Y-m-d h:i')}}.</p>
                            </div>
                        </div>
                    </div>
              
    {% endfor %}
    
    </div>

   </div>
        </section>
    </main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/services.htm", "");
    }
}
