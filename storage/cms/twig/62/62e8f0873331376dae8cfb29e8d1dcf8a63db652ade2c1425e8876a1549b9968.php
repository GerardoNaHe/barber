<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/Index.htm */
class __TwigTemplate_4aab1aeb69eae9b330b398417deea7177bb8d513fc6b6109f56de37417848242 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"preloader-active\">
        <div class=\"preloader d-flex align-items-center justify-content-center\">
            <div class=\"preloader-inner position-relative\">
                <div class=\"preloader-circle\"></div>
                <div class=\"preloader-img pere-text\">
                    <img src=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/logo/loder.png");
        echo "\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <main>
        <!--? slider Area Start-->
        <div class=\"slider-area position-relative fix\">
            <div class=\"slider-active\">
                <!-- Single Slider -->
                <div class=\"single-slider slider-height d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xl-8 col-lg-9 col-md-11 col-sm-10\">
                                <div class=\"hero__caption\">
                                    <span data-animation=\"fadeInUp\" data-delay=\"0.2s\">with patrick potter</span>
                                    <h1 data-animation=\"fadeInUp\" data-delay=\"0.5s\">Our Hair Style make your look elegance</h1>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
                <!-- Single Slider -->
                <div class=\"single-slider slider-height d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xl-8 col-lg-9 col-md-11 col-sm-10\">
                                <div class=\"hero__caption\">
                                    <span data-animation=\"fadeInUp\" data-delay=\"0.2s\">with patrick potter</span>
                                    <h1 data-animation=\"fadeInUp\" data-delay=\"0.5s\">Our Hair Style make your look elegance</h1>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
            <!-- stroke Text -->
            <div class=\"stock-text\">
                <h2>Get More confident</h2>
                <h2>Get More confident</h2>
            </div>
             <!-- Arrow -->
             <div class=\"thumb-content-box\">
                <div class=\"thumb-content\">
                    <h3>make an appointment now</h3>
                    <a href=\"#\"> <i class=\"fas fa-long-arrow-alt-right\"></i></a>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!--? About Area Start -->
        <section class=\"about-area section-padding30 position-relative\" >
            <div class=\"container\">
                <div class=\"row align-items-center\">
                    <div class=\"col-lg-6 col-md-11\">
                        <!-- about-img -->
                        <div class=\"about-img \">
                            <img src=\"";
        // line 64
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/about.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                    <div class=\"col-lg-6 col-md-12\">
                        <div class=\"about-caption\">
                            <!-- Section Tittle -->
                            <div class=\"section-tittle section-tittle3 mb-35\">
                                <span>About Our company</span>
                                <h2>52 Years Of Experience In Hair cut!</h2>
                            </div>
                            <p class=\"mb-30 pera-bottom\">Brook presents your services with flexible, convenient and cdpoe layouts. You can select your favorite layouts & elements for cular ts with unlimited ustomization possibilities. Pixel-perfreplication of the designers is intended.</p>
                            <p class=\"pera-top mb-50\">Brook presents your services with flexible, convefnient and ent anipurpose layouts. You can select your favorite.</p>
                            <img src=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/signature.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Shape -->
            <div class=\"about-shape\">
                <img src=\"";
        // line 83
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/about-shape.png");
        echo "\" alt=\"\">
            </div>
        </section>
        <!-- About-2 Area End -->
        <!--? Services Area Start -->
        <section class=\"service-area pb-170\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                ";
        // line 100
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 100);
        // line 101
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 101);
        // line 102
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 102);
        // line 103
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 103);
        // line 104
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 104);
        // line 105
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 105);
        // line 106
        echo "
<div class=\"row\">
    ";
        // line 108
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 109
            echo "        <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                 <h4><a href=\"";
            // line 115
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("service", ["slug" => twig_get_attribute($this->env, $this->source, $context["record"], "slug", [], "any", false, false, false, 115)]);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "title", [], "any", false, false, false, 115), "html", null, true);
            echo "</a></h4>
                                <h4>";
            // line 116
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "category", [], "any", false, false, false, 116), "title", [], "any", false, false, false, 116), "html", null, true);
            echo "</h4>

                                <p>";
            // line 118
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 118), "html", null, true);
            echo ".</p>
                                <p>";
            // line 119
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "date", [], "any", false, false, false, 119), "Y-m-d h:i"), "html", null, true);
            echo ".</p>
                            </div>
                        </div>
                    </div>
    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "
               <!--  <div class=\"row\">
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Stylish Hair Cut</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption active text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-fitness\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Body Massege</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div> 
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-clock\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Breard Style</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            
        </section>
        <!-- Services Area End -->
        <!--? Team Start -->
        <div class=\"team-area pb-180\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-8 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>Professional Teams</span>
                            <h2>Our award winner hair cut exparts for you</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row team-active dot-style\">
                    <!-- single Tem -->
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 182
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team1.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Guy C. Pulido bks</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 193
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team2.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Color Expart</span>
                                <h3><a href=\"#\">Steve L. Nolan</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 204
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team3.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"";
        // line 215
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/team2.png");
        echo "\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!-- Best Pricing Area Start -->
        <div class=\"best-pricing section-padding2 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row justify-content-end\">
                    <div class=\"col-xl-7 col-lg-7\">
                        <div class=\"section-tittle mb-50\">
                            <span>Our Best Pricing</span>
                            <h2>We provide best price<br> in the city!</h2>
                        </div>
                        <!-- Pricing  -->
                        <div class=\"row\">
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Styling. . . . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$25</span></li>
                                        <li>Styling + Color. . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Styling + Tint. . . . . . . . . . . . . . . . . . . . . .<span>\$65</span></li>
                                        <li>  Semi-permanent wave. . . . . . . . . . . . .<span>\$65</span></li>
                                        <li> Cut + Styling. . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li> Cut + Styling + Color. . . . . . . . . . . . . <span>\$100</span></li>
                                        <li> Cut + Styling + Tint. . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Cut. . . . . . . . . . . . . . . . . . . . . . . . . . . . .<span>\$25</span></li>
                                        <li>Shave. . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Beard trim. . . . . . . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + beard trim. . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + shave. . . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li>Clean up. . . . . . . . . . . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- pricing img -->
            <div class=\"pricing-img\">
                <img class=\"pricing-img1\" src=\"";
        // line 269
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/pricing1.png");
        echo "\" alt=\"\">
                <img class=\"pricing-img2\" src=\"";
        // line 270
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/pricing2.png");
        echo "\" alt=\"\">
            </div>
        </div>
        <!-- Best Pricing Area End -->
        <!--? Gallery Area Start -->
        
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-9 col-sm-10\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>our image gellary</span>
                            <h2>some images from our barber shop</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 290
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery1.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 296
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery2.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 302
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery3.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url(";
        // line 308
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/gallery4.png");
        echo ");\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
        <!-- Cut Details Start -->
        <div class=\"cut-details section-bg section-padding2\" data-background=\"";
        // line 317
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/section_bg02.png");
        echo "\">
           <div class=\"container\">
            <div class=\"cut-active dot-style\">
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
            </div>
           </div>
        </div>
        <!-- Cut Details End -->
        <!--? Blog Area Start -->
        <section class=\"home-blog-area section-padding30\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-10 col-sm-10\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>our recent news</span>
                            <h2>Hipos and tricks from recent blog</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xl-6 col-lg-6 col-md-6\">
                        <div class=\"home-blog-single mb-30\">
                            <div class=\"blog-img-cap\">
                                <div class=\"blog-img\">
                                    <img src=\"";
        // line 374
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/home-blog1.png");
        echo "\" alt=\"\">
                                    <!-- Blog date -->
                                    <div class=\"blog-date text-center\">
                                        <span>24</span>
                                        <p>Now</p>
                                    </div>
                                </div>
                                <div class=\"blog-cap\">
                                    <p>|   Physics</p>
                                    <h3><a href=\"blog_details.html\">Footprints in Time is perfect House in Kurashiki</a></h3>
                                    <a href=\"blog_details.html\" class=\"more-btn\">became a member »</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-6 col-lg-6 col-md-6\">
                        <div class=\"home-blog-single mb-30\">
                            <div class=\"blog-img-cap\">
                                <div class=\"blog-img\">
                                    <img src=\"";
        // line 393
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/home-blog2.png");
        echo "\" alt=\"\">
                                    <!-- Blog date -->
                                    <div class=\"blog-date text-center\">
                                        <span>24</span>
                                        <p>Now</p>
                                    </div>
                                </div>
                                <div class=\"blog-cap\">
                                    <p>|   Physics</p>
                                    <h3><a href=\"blog_details.html\">Footprints in Time is perfect House in Kurashiki</a></h3>
                                    <a href=\"blog_details.html\" class=\"more-btn\">became a member »</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Area End -->
    </main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/Index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  511 => 393,  489 => 374,  429 => 317,  417 => 308,  408 => 302,  399 => 296,  390 => 290,  367 => 270,  363 => 269,  306 => 215,  292 => 204,  278 => 193,  264 => 182,  205 => 125,  193 => 119,  189 => 118,  184 => 116,  178 => 115,  170 => 109,  166 => 108,  162 => 106,  160 => 105,  158 => 104,  156 => 103,  154 => 102,  152 => 101,  150 => 100,  130 => 83,  120 => 76,  105 => 64,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"preloader-active\">
        <div class=\"preloader d-flex align-items-center justify-content-center\">
            <div class=\"preloader-inner position-relative\">
                <div class=\"preloader-circle\"></div>
                <div class=\"preloader-img pere-text\">
                    <img src=\"{{'assets/img/logo/loder.png'|theme}}\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <main>
        <!--? slider Area Start-->
        <div class=\"slider-area position-relative fix\">
            <div class=\"slider-active\">
                <!-- Single Slider -->
                <div class=\"single-slider slider-height d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xl-8 col-lg-9 col-md-11 col-sm-10\">
                                <div class=\"hero__caption\">
                                    <span data-animation=\"fadeInUp\" data-delay=\"0.2s\">with patrick potter</span>
                                    <h1 data-animation=\"fadeInUp\" data-delay=\"0.5s\">Our Hair Style make your look elegance</h1>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
                <!-- Single Slider -->
                <div class=\"single-slider slider-height d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xl-8 col-lg-9 col-md-11 col-sm-10\">
                                <div class=\"hero__caption\">
                                    <span data-animation=\"fadeInUp\" data-delay=\"0.2s\">with patrick potter</span>
                                    <h1 data-animation=\"fadeInUp\" data-delay=\"0.5s\">Our Hair Style make your look elegance</h1>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
            <!-- stroke Text -->
            <div class=\"stock-text\">
                <h2>Get More confident</h2>
                <h2>Get More confident</h2>
            </div>
             <!-- Arrow -->
             <div class=\"thumb-content-box\">
                <div class=\"thumb-content\">
                    <h3>make an appointment now</h3>
                    <a href=\"#\"> <i class=\"fas fa-long-arrow-alt-right\"></i></a>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!--? About Area Start -->
        <section class=\"about-area section-padding30 position-relative\" >
            <div class=\"container\">
                <div class=\"row align-items-center\">
                    <div class=\"col-lg-6 col-md-11\">
                        <!-- about-img -->
                        <div class=\"about-img \">
                            <img src=\"{{'assets/img/gallery/about.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                    <div class=\"col-lg-6 col-md-12\">
                        <div class=\"about-caption\">
                            <!-- Section Tittle -->
                            <div class=\"section-tittle section-tittle3 mb-35\">
                                <span>About Our company</span>
                                <h2>52 Years Of Experience In Hair cut!</h2>
                            </div>
                            <p class=\"mb-30 pera-bottom\">Brook presents your services with flexible, convenient and cdpoe layouts. You can select your favorite layouts & elements for cular ts with unlimited ustomization possibilities. Pixel-perfreplication of the designers is intended.</p>
                            <p class=\"pera-top mb-50\">Brook presents your services with flexible, convefnient and ent anipurpose layouts. You can select your favorite.</p>
                            <img src=\"{{'assets/img/gallery/signature.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Shape -->
            <div class=\"about-shape\">
                <img src=\"{{'assets/img/gallery/about-shape.png'|theme}}\" alt=\"\">
            </div>
        </section>
        <!-- About-2 Area End -->
        <!--? Services Area Start -->
        <section class=\"service-area pb-170\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row d-flex justify-content-center\">
                    <div class=\"col-xl-7 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>Professional Services</span>
                            <h2>Our Best services that  we offering to you</h2>
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                {% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

<div class=\"row\">
    {% for record in records %}
        <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                 <h4><a href=\"{{'service'|page({slug:record.slug})}}\">{{record.title}}</a></h4>
                                <h4>{{record.category.title}}</h4>

                                <p>{{record.description}}.</p>
                                <p>{{record.date|date('Y-m-d h:i')}}.</p>
                            </div>
                        </div>
                    </div>
    
    {% endfor %}

               <!--  <div class=\"row\">
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-healthcare-and-medical\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Stylish Hair Cut</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption active text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-fitness\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Body Massege</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div> 
                    <div class=\"col-xl-4 col-lg-4 col-md-6\">
                        <div class=\"services-caption text-center mb-30\">
                            <div class=\"service-icon\">
                                <i class=\"flaticon-clock\"></i>
                            </div> 
                            <div class=\"service-cap\">
                                <h4><a href=\"#\">Breard Style</a></h4>
                                <p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            
        </section>
        <!-- Services Area End -->
        <!--? Team Start -->
        <div class=\"team-area pb-180\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-8 col-lg-8 col-md-11 col-sm-11\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>Professional Teams</span>
                            <h2>Our award winner hair cut exparts for you</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row team-active dot-style\">
                    <!-- single Tem -->
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team1.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Guy C. Pulido bks</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team2.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Color Expart</span>
                                <h3><a href=\"#\">Steve L. Nolan</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team3.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-\">
                        <div class=\"single-team mb-80 text-center\">
                            <div class=\"team-img\">
                                <img src=\"{{'assets/img/gallery/team2.png'|theme}}\" alt=\"\">
                            </div>
                            <div class=\"team-caption\">
                                <span>Master Barber</span>
                                <h3><a href=\"#\">Edgar P. Mathis</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!-- Best Pricing Area Start -->
        <div class=\"best-pricing section-padding2 position-relative\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <div class=\"row justify-content-end\">
                    <div class=\"col-xl-7 col-lg-7\">
                        <div class=\"section-tittle mb-50\">
                            <span>Our Best Pricing</span>
                            <h2>We provide best price<br> in the city!</h2>
                        </div>
                        <!-- Pricing  -->
                        <div class=\"row\">
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Styling. . . . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$25</span></li>
                                        <li>Styling + Color. . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Styling + Tint. . . . . . . . . . . . . . . . . . . . . .<span>\$65</span></li>
                                        <li>  Semi-permanent wave. . . . . . . . . . . . .<span>\$65</span></li>
                                        <li> Cut + Styling. . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li> Cut + Styling + Color. . . . . . . . . . . . . <span>\$100</span></li>
                                        <li> Cut + Styling + Tint. . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"col-lg-6 col-md-6 col-sm-6\">
                                <div class=\"pricing-list\">
                                    <ul>
                                        <li>Cut. . . . . . . . . . . . . . . . . . . . . . . . . . . . .<span>\$25</span></li>
                                        <li>Shave. . . . . . . . . . . . . . . . . . . . . . . . . . <span>\$65</span></li>
                                        <li>Beard trim. . . . . . . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + beard trim. . . . . . . . . . . . . . . . .  <span>\$65</span></li>
                                        <li>Cut + shave. . . . . . . . . . . . . . . . . . . . . . .<span>\$63</span></li>
                                        <li>Clean up. . . . . . . . . . . . . . . . . . . . . . . . .<span>\$100</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- pricing img -->
            <div class=\"pricing-img\">
                <img class=\"pricing-img1\" src=\"{{'assets/img/gallery/pricing1.png'|theme}}\" alt=\"\">
                <img class=\"pricing-img2\" src=\"{{'assets/img/gallery/pricing2.png'|theme}}\" alt=\"\">
            </div>
        </div>
        <!-- Best Pricing Area End -->
        <!--? Gallery Area Start -->
        
        <div class=\"gallery-area section-padding30\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-9 col-sm-10\">
                        <div class=\"section-tittle text-center mb-100\">
                            <span>our image gellary</span>
                            <h2>some images from our barber shop</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery1.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery2.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery3.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                    <div class=\"col-lg-4 col-md-6 col-sm-6\">
                        <div class=\"box snake mb-30\">
                            <div class=\"gallery-img \" style=\"background-image: url({{'assets/img/gallery/gallery4.png'|theme}});\"></div>
                            <div class=\"overlay\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
        <!-- Cut Details Start -->
        <div class=\"cut-details section-bg section-padding2\" data-background=\"{{'assets/img/gallery/section_bg02.png'|theme}}\">
           <div class=\"container\">
            <div class=\"cut-active dot-style\">
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
                <div class=\"single-cut\">
                    <div class=\"cut-icon mb-20\">
                        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"50px\" height=\"50px\">
                            <image  x=\"0px\" y=\"0px\" width=\"50px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkBQ4MDDIERuyfAAADc0lEQVRYw7WYXWxTZRjH/+e0ikhh7QgfiYJZZ7bhBC6mU0LQ6DBADNGYLEaNJGpi4jTEQczYjQG8EL2ThAUTvTRGBwmECyBA+XRKHJpUL1yXFseWbe1ixgZCSAg/Lmo9bXe+up0+/5vT//Oc9/ee8z7nqwbyGbVqUL2iiuiurmtMKf2tu/52DXtW1OhVtekFRZTSkCY1rYcV0VI1arl+VULH9JvnGLhpHT/wD728z+M22QVs5ksyJOlkgds4zqlWEgzSQQ3uEzF4ju8ZpZsHK4NEOcgo7xL2AFhq4CgDtPmHPEWGg0R9AwrayjD77CY2s/RtsrRXDMhrCSc5wyIvyE6GaJ4lQogQB/idZW6QjxlkxRwQee0lWdoupec0a9uqlauHM8VrYyXqyLIuEIQIcYLPZ0JC/EJnQIh8C4xYDV0wO0hgBAgRm0kxrxhSS46mQBFCHKa7GLKbbwNHiCayRAqQCBMBdVW5etlRgGzjWFUQYgMDGHnIaZfbSIxTWNFP3MGzl0GaViQWMVXoAhv9SGn0O3hO+oLPkHiZ4y5FacrD3nPSJn5GptbrJ7+P+VnERa3VA6bWKFlFyC0NqdFUXOkqQqS06kwt1XhVIeNaZiqqSZeS0z4955jWwrBCuudSskvSRklSTDEXzznuaJ74l/m+rt4Wm3Zt8WxhcYAOU5Na7OuwJ3165RHTlKlhrfQFaZckXfH0ymOFhsNKaZX6POYSU7v2SZJ6XTz7aFJKbKfH9ZxuLLp9pIk5evaKM4ZMndXzrjOJ/7+V0Uv/rYKdZx9tOi8Jg3HqPY+kn66iGdt59jrMe/nnyX52V+mhVcsNFuchLWQqeH+vRB9xCBVeJC7xZhUQYTKstyBb+JNQ4JB3OJvfKhgJPggYEeEaz5ZCmpgI4H2+WD18Xdi2zG4uBbj8r5GxvtUs2+AE+wNCrCZHq/W7OBUlya4AEI9yjbeKnfL0VbrmiIgzyCelXnnJI/zBV3NYm6cZoaPcnVkW4yQXZtVpBp1keWVmxq7YpIsc2ys8nmbOc5k6u5zTLqtIkOQNn/eBer4hx4eY9nm3XbdwkTSfun67PEQ7R8ixh1rnKsPj/64WbdPrmtI5XdGAruqGrmu+IlquBj2hDXpGl/WdDumm2yBeEEky9KRe1Go16jFFFNVt3dSEUvpLfbqgae8B7gNdcvnkrRzZ4gAAAABJRU5ErkJggg==\" />
                        </svg>
                    </div>
                    <div class=\"cut-descriptions\">
                        <p>Vestibulum varius, velit sit amet tempor efficitur, ligula mi lacinia libero, vehicula dui nisi eget purus. Integer cursus nibh non risus maximus dictum. Suspendis.</p>
                        <span>JONT NICOLIN KOOK</span>
                    </div>
                </div>
            </div>
           </div>
        </div>
        <!-- Cut Details End -->
        <!--? Blog Area Start -->
        <section class=\"home-blog-area section-padding30\" style=\"background-color: #F6CD37\">
            <div class=\"container\">
                <!-- Section Tittle -->
                <div class=\"row justify-content-center\">
                    <div class=\"col-xl-6 col-lg-7 col-md-10 col-sm-10\">
                        <div class=\"section-tittle text-center mb-90\">
                            <span>our recent news</span>
                            <h2>Hipos and tricks from recent blog</h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xl-6 col-lg-6 col-md-6\">
                        <div class=\"home-blog-single mb-30\">
                            <div class=\"blog-img-cap\">
                                <div class=\"blog-img\">
                                    <img src=\"{{'assets/img/gallery/home-blog1.png'|theme}}\" alt=\"\">
                                    <!-- Blog date -->
                                    <div class=\"blog-date text-center\">
                                        <span>24</span>
                                        <p>Now</p>
                                    </div>
                                </div>
                                <div class=\"blog-cap\">
                                    <p>|   Physics</p>
                                    <h3><a href=\"blog_details.html\">Footprints in Time is perfect House in Kurashiki</a></h3>
                                    <a href=\"blog_details.html\" class=\"more-btn\">became a member »</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xl-6 col-lg-6 col-md-6\">
                        <div class=\"home-blog-single mb-30\">
                            <div class=\"blog-img-cap\">
                                <div class=\"blog-img\">
                                    <img src=\"{{'assets/img/gallery/home-blog2.png'|theme}}\" alt=\"\">
                                    <!-- Blog date -->
                                    <div class=\"blog-date text-center\">
                                        <span>24</span>
                                        <p>Now</p>
                                    </div>
                                </div>
                                <div class=\"blog-cap\">
                                    <p>|   Physics</p>
                                    <h3><a href=\"blog_details.html\">Footprints in Time is perfect House in Kurashiki</a></h3>
                                    <a href=\"blog_details.html\" class=\"more-btn\">became a member »</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Area End -->
    </main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/Index.htm", "");
    }
}
