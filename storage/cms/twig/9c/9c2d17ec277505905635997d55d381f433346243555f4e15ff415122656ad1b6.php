<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/layouts/default.htm */
class __TwigTemplate_8fa80ff6aae528e3948d329cf67135c8a5a7449f161610ac447f6b8b00f8d0aa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html class=\"no-js\" lang=\"zxx\">

<head>
 ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        echo "  
</head> 



<body style=\"background-color: #F6CD37\">
    ";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 11
        echo "
    <header>
        ";
        // line 13
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 14
        echo "    </header> 

    <footer>
        ";
        // line 17
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 18
        echo "    </footer>
    
    <div id=\"back-top\" >
        <a title=\"Go to Top\" href=\"#\"> <i class=\"fas fa-level-up-alt\"></i></a>
    </div>

    <!-- JS here -->
    <script src=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/vendor/modernizr-3.5.0.min.js");
        echo "\"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src=\"";
        // line 27
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/vendor/jquery-1.12.4.min.js");
        echo "\"></script>
    <script src=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/popper.min.js");
        echo "\"></script>
    <script src=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/bootstrap.min.js");
        echo "\"></script>
    <!-- Jquery Mobile Menu -->
    <script src=\"";
        // line 31
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.slicknav.min.js");
        echo "\"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src=\"";
        // line 34
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/owl.carousel.min.js");
        echo "\"></script>
    <script src=\"";
        // line 35
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/slick.min.js");
        echo "\"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src=\"";
        // line 37
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/wow.min.js");
        echo "\"></script>
    <script src=\"";
        // line 38
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/animated.headline.js");
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.magnific-popup.js");
        echo "\"></script>

    <!-- Date Picker -->
    <script src=\"";
        // line 42
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/gijgo.min.js");
        echo "\"></script>
    <!-- Nice-select, sticky -->
    <script src=\"";
        // line 44
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.nice-select.min.js");
        echo "\"></script>
    <script src=\"";
        // line 45
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.sticky.js");
        echo "\"></script>
    
    <!-- counter , waypoint,Hover Direction -->
    <script src=\"";
        // line 48
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.counterup.min.js");
        echo "\"></script>
    <script src=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/waypoints.min.js");
        echo "\"></script>
    <script src=\"";
        // line 50
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/jquery.countdown.min.js");
        echo "\"></script>
    <script src=\"";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/hover-direction-snake.min.js");
        echo "\"></script>

    <!-- contact js -->
    <script src=\"./assets/js/contact.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.form.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.validate.min.js'| theme}}\"></script>
    <script src=\"./assets/js/mail-script.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.ajaxchimp.min.js'| theme}}\"></script>
    
    <!-- Jquery Plugins, main Jquery -->    
    <script src=\"";
        // line 61
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/plugins.js");
        echo "\"></script>
    <script src=\"";
        // line 62
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("./assets/js/main.js");
        echo "\"></script>
</body>

</html>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 62,  168 => 61,  155 => 51,  151 => 50,  147 => 49,  143 => 48,  137 => 45,  133 => 44,  128 => 42,  122 => 39,  118 => 38,  114 => 37,  109 => 35,  105 => 34,  99 => 31,  94 => 29,  90 => 28,  86 => 27,  81 => 25,  72 => 18,  68 => 17,  63 => 14,  59 => 13,  55 => 11,  53 => 10,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<html class=\"no-js\" lang=\"zxx\">

<head>
 {% partial \"head\" %}  
</head> 



<body style=\"background-color: #F6CD37\">
    {% page %}

    <header>
        {% partial \"header\" %}
    </header> 

    <footer>
        {% partial \"footer\" %}
    </footer>
    
    <div id=\"back-top\" >
        <a title=\"Go to Top\" href=\"#\"> <i class=\"fas fa-level-up-alt\"></i></a>
    </div>

    <!-- JS here -->
    <script src=\"{{'./assets/js/vendor/modernizr-3.5.0.min.js'| theme}}\"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src=\"{{'./assets/js/vendor/jquery-1.12.4.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/popper.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/bootstrap.min.js'| theme}}\"></script>
    <!-- Jquery Mobile Menu -->
    <script src=\"{{'./assets/js/jquery.slicknav.min.js'| theme}}\"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src=\"{{'./assets/js/owl.carousel.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/slick.min.js'| theme}}\"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src=\"{{'./assets/js/wow.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/animated.headline.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/jquery.magnific-popup.js'| theme}}\"></script>

    <!-- Date Picker -->
    <script src=\"{{'./assets/js/gijgo.min.js'| theme}}\"></script>
    <!-- Nice-select, sticky -->
    <script src=\"{{'./assets/js/jquery.nice-select.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/jquery.sticky.js'| theme}}\"></script>
    
    <!-- counter , waypoint,Hover Direction -->
    <script src=\"{{'./assets/js/jquery.counterup.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/waypoints.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/jquery.countdown.min.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/hover-direction-snake.min.js'| theme}}\"></script>

    <!-- contact js -->
    <script src=\"./assets/js/contact.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.form.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.validate.min.js'| theme}}\"></script>
    <script src=\"./assets/js/mail-script.js'| theme}}\"></script>
    <script src=\"./assets/js/jquery.ajaxchimp.min.js'| theme}}\"></script>
    
    <!-- Jquery Plugins, main Jquery -->    
    <script src=\"{{'./assets/js/plugins.js'| theme}}\"></script>
    <script src=\"{{'./assets/js/main.js'| theme}}\"></script>
</body>

</html>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/layouts/default.htm", "");
    }
}
