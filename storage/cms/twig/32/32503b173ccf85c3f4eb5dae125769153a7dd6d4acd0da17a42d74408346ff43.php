<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/service.htm */
class __TwigTemplate_95dc1ecf9ada9ce390e8759071c0da339c4f73f30b3ec10619f288f6122ab789 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["record"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "record", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "notFoundMessage", [], "any", false, false, false, 3);
        // line 4
        echo "
 <main>
 <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "title", [], "any", false, false, false, 13), "html", null, true);
        echo "</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
\t\t<!--? Start Sample Area -->
 <section class=\"sample-text-area\" style=\"background-color: #F6CD37\">
\t\t\t<div class=\"container box_1170\">
\t\t\t\t<h3 class=\"text-heading\">";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "description", [], "any", false, false, false, 24), "html", null, true);
        echo "</h3>
\t\t\t\t<h3 class=\"text-heading\">";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "date", [], "any", false, false, false, 25), "html", null, true);
        echo "</h3>
\t\t\t\t<h3 class=\"text-heading\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "location", [], "any", false, false, false, 26), "html", null, true);
        echo "</h3>
\t\t\t\t<p class=\"sample-text\">";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "content", [], "any", false, false, false, 27), "html", null, true);
        echo "</p>
\t\t\t</div>
\t\t</section>
\t\t<!-- End Sample Area -->\t
\t</main>";
    }

    public function getTemplateName()
    {
        return "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/service.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 27,  76 => 26,  72 => 25,  68 => 24,  54 => 13,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}

 <main>
 <!--? Hero Start -->
        <div class=\"slider-area2\">
            <div class=\"slider-height2 d-flex align-items-center\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xl-12\">
                            <div class=\"hero-cap hero-cap2 pt-70 text-center\">
                                <h2>{{record.title}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
\t\t<!--? Start Sample Area -->
 <section class=\"sample-text-area\" style=\"background-color: #F6CD37\">
\t\t\t<div class=\"container box_1170\">
\t\t\t\t<h3 class=\"text-heading\">{{record.description}}</h3>
\t\t\t\t<h3 class=\"text-heading\">{{record.date}}</h3>
\t\t\t\t<h3 class=\"text-heading\">{{record.location}}</h3>
\t\t\t\t<p class=\"sample-text\">{{record.content}}</p>
\t\t\t</div>
\t\t</section>
\t\t<!-- End Sample Area -->\t
\t</main>", "/Applications/XAMPP/xamppfiles/htdocs/Sitios/Prueba/local.barber.com/themes/prueba/pages/service.htm", "");
    }
}
