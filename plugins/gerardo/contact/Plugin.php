<?php namespace gerardo\contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'gerardo\contact\components\ContactForm' => 'contactform',
    	];
    }

    public function registerSettings()
    {
    }
}
