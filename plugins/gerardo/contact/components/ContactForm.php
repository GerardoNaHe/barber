<?php namespace gerardo\contact\components;

use Cms\classes\ComponentBase;

use Mail;
use Input;

Use Validator;
use Redirect;

class ContactForm extends ComponentBase {

   public function ComponentDetails(){
     
     return[
      
      'name' => 'Simple contact form',
      'description' => 'simple contact form'

     ];

  }

   public function onSend(){

           $vars = ['name' => Input::get('name'), 'email' => Input::get('email') , 'content' => Input::get('content') ];

          Mail::send('gerardo.contact::mail.message', $vars, function($message) {

          $message->to('admin@domain.tld', 'Admin Person');
          $message->subject('New message from contact form');

          });
 
        }

}