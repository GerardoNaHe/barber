<?php
namespace gerardo\services\components;

use   gerardo\services\Models\Service;
use  gerardo\services\Models\Category;

use Lang;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\Builder\Classes\ComponentHelper;
use SystemException;
use Exception;

class ServiceList extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Service',
            'description' => 'Lista de todos los servicios.'
        ];
    }

    public function defineProperties()
{
    return [
        'category' => [
            'title'       => 'category',
            'type'        => 'dropdown',
            'default'     => ''
        ],
    ];
}

public function getCategoryOptions(){
	$result= [];
	$result[''] = 'sin categoria';
	$data=Category::get();

	foreach ($data as $item) {
		$result[$item->id] = $item->title;
	}
	return $result;
}

   public function onRun(){

      $category = $this->property('category');

      if($category !=''){
       $this->page['result'] = Service::where('category_id', $category)->get();
      }else{
      	$this->page['result'] = Service::get();
       }
     }
    

}

?>