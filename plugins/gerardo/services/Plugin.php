<?php namespace Gerardo\Services;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
       return [
        'gerardo\services\components\ServiceList' => 'service'
    ];

    }

    public function registerSettings()
    {
    }
}
