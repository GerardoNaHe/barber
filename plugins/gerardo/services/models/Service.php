<?php namespace Gerardo\Services\Models;

use Model;
use   gerardo\services\Models\Service;

use  gerardo\services\Models\category;


/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'gerardo_services_services';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title'    => 'required',
        'slug'     => 'required',
        'status'   => 'required',
        'category'   => 'required',
        'description'   => 'required',
        
    ];

    public $belongsTo = [
        'category' => 'gerardo\Services\Models\category'

    ];

    public $attachOne = [
    'picture' => 'System\Models\File'

];


public function scopeStatusPopular($query,$slug){ 
     //slug es el id del servicio.

    if($slug =='todos'){
      
      return $query->where('status',1);

    }else{
    $cat=category::where('slug',$slug)->first();
    return $query->where('category_id',$cat->id)->where('status',1);
      
     }
    
}


}