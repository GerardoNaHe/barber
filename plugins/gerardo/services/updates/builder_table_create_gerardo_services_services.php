<?php namespace Gerardo\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGerardoServicesServices extends Migration
{
    public function up()
    {
        Schema::create('gerardo_services_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 50);
            $table->string('slug', 75);
            $table->string('description', 100);
            $table->text('content');
            $table->dateTime('date');
            $table->integer('category_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('gerardo_services_services');
    }
}
