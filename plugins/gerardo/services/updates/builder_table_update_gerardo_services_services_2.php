<?php namespace Gerardo\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGerardoServicesServices2 extends Migration
{
    public function up()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->integer('category_id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->integer('category_id')->unsigned()->change();
        });
    }
}
