<?php namespace Gerardo\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGerardoServicesServices4 extends Migration
{
    public function up()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->dropColumn('picture');
        });
    }
    
    public function down()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->string('picture', 100)->nullable();
        });
    }
}
