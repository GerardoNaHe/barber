<?php namespace Gerardo\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGerardoServicesServices5 extends Migration
{
    public function up()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->dropColumn('status');
        });
    }
}
