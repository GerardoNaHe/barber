<?php namespace Gerardo\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGerardoServicesServices extends Migration
{
    public function up()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->string('location', 100);
            $table->string('picture', 100);
        });
    }
    
    public function down()
    {
        Schema::table('gerardo_services_services', function($table)
        {
            $table->dropColumn('location');
            $table->dropColumn('picture');
        });
    }
}
